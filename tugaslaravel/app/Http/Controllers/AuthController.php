<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio ()
    {
        return view('page.form');
    }
    public function welcome ()
    {
        return view('page.welcome');
    }
}
